<?php

namespace App\Form;

use App\Model\Season;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeasonType extends AbstractType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Season::class,
        ));
    }

    public function getParent()
    {
        return HiddenType::class;
    }

}