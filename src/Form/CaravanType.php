<?php

namespace App\Form;

use App\Model\CaravanPriceComposition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CaravanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rent', PriceType::class)
            ->add('touristTax', PriceType::class)
            ->add('bedlinen', PriceType::class)
            ->add('cleanup', PriceType::class)
            ->add('visitor', PriceType::class)
            ->add('season', SeasonType::class)
            ->add('save', SubmitType::class)
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CaravanPriceComposition::class,
        ));
    }
}