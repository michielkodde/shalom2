<?php

namespace App\Form;

use App\Model\TentPriceComposition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rent',PriceType::class)
            ->add('touristTax',PriceType::class)
            ->add('extraPerson',PriceType::class)
            ->add('extraTent',PriceType::class)
            ->add('electricity',PriceType::class)
            ->add('extraCar',PriceType::class)
            ->add('pet',PriceType::class)
            ->add('visitor',PriceType::class)
            ->add('season', SeasonType::class)
            ->add('save', SubmitType::class)
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => TentPriceComposition::class,
        ));
    }
}