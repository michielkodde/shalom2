<?php
/**
 * Created by PhpStorm.
 * User: michiel
 * Date: 30-5-18
 * Time: 21:08
 */

namespace App\Service;


use App\Model\AbstractPriceComposition;
use App\Repository\ApartmentRepository;
use App\Repository\CaravanRepository;
use App\Repository\TentRepository;

class PriceRenderService
{
    /**
     * @var AbstractPriceComposition
     */
    private $composition = null;

    private $apartmentRepo;

    private $tentRepo;

    private $caravanRepo;

    public function __construct(
        CaravanRepository $caravanRepository,
        TentRepository $tentRepository,
        ApartmentRepository $apartmentRepository
    ) {
        $this->apartmentRepo = $apartmentRepository;
        $this->caravanRepo = $caravanRepository;
        $this->tentRepo = $tentRepository;
    }

    private $type = 'apartment';

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function load($season = null)
    {
        switch ($this->type) {
            case 'apartment':
                $this->composition = $this->apartmentRepo->find();
                break;

            case 'tent':
                $this->composition = $this->tentRepo->find($season);
                    break;

            case 'caravan':
                $this->composition = $this->caravanRepo->find($season);
        }
        return $this;
    }

    private static $preferredOrder = [
        'tent' => [
            'rent',
            'extraPerson',
            'extraTent',
            'electricity',
            'extraCar',
            'touristTax',
            'pet',
            'visitor'
        ],
        'caravan' => [
            'rent',
            'cleanup',
            'touristTax',
            'visitor',
            'bedlinen'
        ],
        'apartment' => [
            'rent',
            'cleanup',
            'touristTax',
            'pet',
        ]
    ];

    public function makeIteratable()
    {
        if (is_null($this->composition)) {
            throw new \Exception('first load some prices!');
        }
        $data = (array) $this->composition;
        $normalized = [];

        foreach ($data as $key => $value) {
            $key = preg_replace('/[^\p{L}\p{N}\s]/u', '', $key);
            $normalized[$key] = $value;
        }

        $preferredOrder = self::$preferredOrder[$this->type];

        uksort($normalized, function ($a, $b) use ($preferredOrder) {
            $posA = array_search($a, $preferredOrder);
            $posB = array_search($b, $preferredOrder);
            return $posA - $posB;
        });

        // Unset season
        if (isset($normalized['season'])) {
            unset($normalized['season']);
        }

        return $normalized;
    }
}