<?php

namespace App\Repository;


use App\Model\CaravanPriceComposition;
use App\Model\Season;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;

class CaravanRepository
{
    private $storageLocation;
    private $fileSystem;
    private $serializer;

    public function __construct($storageLocation, FileSystem $fileSystem, Serializer $serializer)
    {
        $this->storageLocation = $storageLocation;
        $this->fileSystem = $fileSystem;
        $this->serializer = $serializer;
    }

    public function find($season)
    {
        Season::assertValidSeason($season);
        $target = $this->getFileName('caravan_' . $season);

        if (!$this->fileSystem->exists($target)){
            return;
        }

        try {
            return $this->serializer->deserialize(
                file_get_contents($target),
                CaravanPriceComposition::class,
                'json'
            );
        } catch (\Exception $e) {
            return null;
        }
    }

    public function save(CaravanPriceComposition $caravanPriceComposition)
    {

        $target = $this->getFileName('caravan_' . $caravanPriceComposition->getSeason()->getSeason());

        if (!$this->fileSystem->exists($target)){
            $this->fileSystem->touch($target);
        }

        $content = $this->serializer->serialize($caravanPriceComposition, 'json');
        file_put_contents($target, $content);
    }

    private function getFileName($target, $extension = '.json')
    {
        return $this->storageLocation . $target . $extension;
    }

}