<?php

namespace App\Repository;


use App\Model\ApartmentPriceComposition;
use App\Model\Season;
use App\Model\TentPriceComposition;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;

class TentRepository
{
    private $storageLocation;
    private $fileSystem;
    private $serializer;

    public function __construct($storageLocation, FileSystem $fileSystem, Serializer $serializer)
    {
        $this->storageLocation = $storageLocation;
        $this->fileSystem = $fileSystem;
        $this->serializer = $serializer;
    }

    /**
     * @param string $season
     * @return null|object|void
     */
    public function find(string $season)
    {
        Season::assertValidSeason($season);
        $target = $this->getFileName('tent_' . (string) $season);
        if (!$this->fileSystem->exists($target)){
            return;
        }

        try {
            return $this->serializer->deserialize(
                file_get_contents($target),
                TentPriceComposition::class,
                'json'
            );
        } catch (\Exception $e) {
            return null;
        }
    }

    public function save(TentPriceComposition $tentPriceComposition)
    {
        $target = $this->getFileName('tent_' . $tentPriceComposition->getSeason()->getSeason());

        if (!$this->fileSystem->exists($target)){
            $this->fileSystem->touch($target);
        }

        $content = $this->serializer->serialize($tentPriceComposition, 'json');
        file_put_contents($target, $content);
    }

    private function getFileName($target, $extension = '.json')
    {
        return $this->storageLocation . $target . $extension;
    }

}