<?php

namespace App\Repository;

use App\Model\ApartmentPriceComposition;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;

class ApartmentRepository
{
    private $storageLocation;
    private $fileSystem;
    private $serializer;

    public function __construct($storageLocation, FileSystem $fileSystem, Serializer $serializer)
    {
        $this->storageLocation = $storageLocation;
        $this->fileSystem = $fileSystem;
        $this->serializer = $serializer;
    }

    public function find()
    {
        $target = $this->getFileName('apartment');

        if (!$this->fileSystem->exists($target)){
            return;
        }

        try {
            return $this->serializer->deserialize(
                file_get_contents($target),
                ApartmentPriceComposition::class,
                'json'
            );
        } catch (\Exception $e) {
            return null;
        }
    }

    public function save(ApartmentPriceComposition $apartmentPriceComposition)
    {
        $target = $this->getFileName('apartment');

        if (!$this->fileSystem->exists($target)){
            $this->fileSystem->touch($target);
        }

        $content = $this->serializer->serialize($apartmentPriceComposition, 'json');
        file_put_contents($target, $content);
    }

    private function getFileName($target, $extension = '.json')
    {
        return $this->storageLocation . $target . $extension;
    }

}