<?php

namespace App\Controller;

use App\Form\ApartmentType;
use App\Form\CaravanType;
use App\Form\TentType;
use App\Model\CaravanPriceComposition;
use App\Model\Price;
use App\Model\Season;
use App\Model\TentPriceComposition;
use App\Repository\ApartmentRepository;
use App\Repository\CaravanRepository;
use App\Repository\TentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdministrationController extends Controller
{
    private $apartmentRepository;
    private $tentRepository;
    private $caravanRepository;

    public function __construct(ApartmentRepository $repository, TentRepository $tentRepo, CaravanRepository $caravanRepo)
    {
        $this->apartmentRepository = $repository;
        $this->tentRepository = $tentRepo;
        $this->caravanRepository = $caravanRepo;
    }

    public function apartmentAction(Request $request)
    {
        $title = 'Appartement';
        $apartment = $this->apartmentRepository->find();
        $form = $this->createForm(ApartmentType::class, $apartment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->apartmentRepository->save($apartment);
        }

        return $this->render('pages/administation.html.twig', ['form' => $form->createView(), 'title' => $title]);
    }

    public function tentAction(Request $request, $season = 'hoogseizoen')
    {

        $title = sprintf('Kampeerplaats (%s)', $season);
        $tent = $this->tentRepository->find($season);

        $form = $this->createForm(TentType::class, $tent);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->tentRepository->save($tent);
        }

        return $this->render('pages/administation.html.twig', ['form' => $form->createView(), 'title' => $title]);
    }

    public function caravanAction(Request $request, $season = 'hoogseizoen')
    {
        $title = sprintf('Stacaravan (%s)', $season);
        $caravan = $this->caravanRepository->find($season);
        $form = $this->createForm(CaravanType::class, $caravan);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->caravanRepository->save($caravan);
        }

        return $this->render('pages/administation.html.twig', ['form' => $form->createView(), 'title' => $title]);
    }
}