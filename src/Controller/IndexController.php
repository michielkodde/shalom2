<?php

namespace App\Controller;

use App\Service\PriceRenderService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    private $priceRenderService;

    public function __construct(PriceRenderService $priceRenderService)
    {
        $this->priceRenderService = $priceRenderService;
    }

    public function homeAction()
    {
        $tentHigh = $this->priceRenderService
            ->setType('tent')
            ->load('hoogseizoen')
            ->makeIteratable();
        $tentLow = $this->priceRenderService
            ->setType('tent')
            ->load('laagseizoen')
            ->makeIteratable();
        $camperHigh = $this->priceRenderService
            ->setType('caravan')
            ->load('hoogseizoen')
            ->makeIteratable();
        $camperLow = $this->priceRenderService
            ->setType('caravan')
            ->load('laagseizoen')
            ->makeIteratable();

        return $this->render('pages/content.html.twig', [
            'tentHigh' => $tentHigh,
            'tentLow' => $tentLow,
            'camperHigh' => $camperHigh,
            'camperLow' => $camperLow,
        ]);
    }

    public function apartmentAction()
    {
        $prices = $this->priceRenderService
            ->setType('apartment')
            ->load()
            ->makeIteratable();

        return $this->render('pages/apartment.html.twig', ['prices' => $prices]);
    }
}