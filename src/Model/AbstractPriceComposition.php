<?php

namespace App\Model;


abstract class AbstractPriceComposition
{
    protected $rent;

    protected $touristTax;

    /**
     * AbstractPriceComposition constructor.
     * @param $rent
     * @param $touristTax
     */
    public function __construct(Price $rent, Price $touristTax)
    {
        $this->rent = $rent;
        $this->touristTax = $touristTax;
    }

    /**
     * @return Price
     */
    public function getRent(): Price
    {
        return $this->rent;
    }

    /**
     * @param Price $rent
     */
    public function setRent(Price $rent): void
    {
        $this->rent = $rent;
    }

    /**
     * @return Price
     */
    public function getTouristTax(): Price
    {
        return $this->touristTax;
    }

    /**
     * @param Price $touristTax
     */
    public function setTouristTax(Price $touristTax): void
    {
        $this->touristTax = $touristTax;
    }
}