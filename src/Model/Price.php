<?php

namespace App\Model;

final class Price
{
    private $title;

    private $price;

    private $information = '';

    /**
     * @param string $title
     * @param string $price
     * @param string $information
     */
    public function __construct($title, $price, $information = '')
    {
        if (!is_string($title) || empty(trim($title))) {
            throw new \InvalidArgumentException('Title must be a non empty string');
        }

        if (!is_string($price) || empty(trim($price))) {
            throw new \InvalidArgumentException('Price must be a non empty string');
        }

        $this->title = $title;
        $this->price = $price;
        $this->information = $information;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = (string)  $title;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price): void
    {
        $this->price = (string) $price;
    }

    /**
     * @return string
     */
    public function getInformation(): string
    {
        return $this->information;
    }

    /**
     * @param string $information
     */
    public function setInformation($information = ''): void
    {
        $this->information = (string)  $information;
    }

}