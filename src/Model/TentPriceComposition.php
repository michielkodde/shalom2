<?php
namespace App\Model;


class TentPriceComposition extends AbstractPriceComposition
{
    protected $season;

    protected $extraPerson;

    protected $extraTent;

    protected $electricity;

    protected $extraCar;

    protected $pet;

    protected $visitor;

    public function __construct(
        Price $rent,
        Price $touristTax,
        Season $season,
        Price $extraPerson,
        Price $extraTent,
        Price $electricity,
        Price $extraCar,
        Price $pet,
        Price $visitor
    ) {
        parent::__construct($rent, $touristTax);
        $this->season = $season;
        $this->extraPerson = $extraPerson;
        $this->extraTent = $extraTent;
        $this->electricity = $electricity;
        $this->extraCar = $extraCar;
        $this->pet = $pet;
        $this->visitor = $visitor;
    }

    /**
     * @return Season
     */
    public function getSeason(): Season
    {
        return $this->season;
    }

    /**
     * @param Season $season
     */
    public function setSeason($season): void
    {
        if (!$season instanceof Season) {
            $season = new Season($season);
        }

        $this->season = $season;
    }

    /**
     * @return Price
     */
    public function getExtraPerson(): Price
    {
        return $this->extraPerson;
    }

    /**
     * @param Price $extraPerson
     */
    public function setExtraPerson(Price $extraPerson): void
    {
        $this->extraPerson = $extraPerson;
    }

    /**
     * @return Price
     */
    public function getExtraTent(): Price
    {
        return $this->extraTent;
    }

    /**
     * @param Price $extraTent
     */
    public function setExtraTent(Price $extraTent): void
    {
        $this->extraTent = $extraTent;
    }

    /**
     * @return Price
     */
    public function getElectricity(): Price
    {
        return $this->electricity;
    }

    /**
     * @param Price $electricity
     */
    public function setElectricity(Price $electricity): void
    {
        $this->electricity = $electricity;
    }

    /**
     * @return Price
     */
    public function getExtraCar(): Price
    {
        return $this->extraCar;
    }

    /**
     * @param Price $extraCar
     */
    public function setExtraCar(Price $extraCar): void
    {
        $this->extraCar = $extraCar;
    }

    /**
     * @return Price
     */
    public function getPet(): Price
    {
        return $this->pet;
    }

    /**
     * @param Price $pet
     */
    public function setPet(Price $pet): void
    {
        $this->pet = $pet;
    }

    /**
     * @return Price
     */
    public function getVisitor(): Price
    {
        return $this->visitor;
    }

    /**
     * @param Price $visitor
     */
    public function setVisitor(Price $visitor): void
    {
        $this->visitor = $visitor;
    }

}