<?php
namespace App\Model;


class ApartmentPriceComposition extends AbstractPriceComposition
{
    protected $cleanup;

    protected $pet;

    public function __construct(Price $rent, Price $touristTax, Price $cleanup, Price $pet)
    {
        parent::__construct($rent, $touristTax);
        $this->cleanup = $cleanup;
        $this->pet = $pet;
    }

    /**
     * @return Price
     */
    public function getCleanup(): Price
    {
        return $this->cleanup;
    }

    /**
     * @return Price
     */
    public function getPet(): Price
    {
        return $this->pet;
    }

    /**
     * @param Price $cleanup
     */
    public function setCleanup(Price $cleanup): void
    {
        $this->cleanup = $cleanup;
    }

    /**
     * @param Price $pet
     */
    public function setPet(Price $pet): void
    {
        $this->pet = $pet;
    }


}