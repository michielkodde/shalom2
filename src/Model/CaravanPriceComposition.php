<?php
namespace App\Model;


class CaravanPriceComposition extends AbstractPriceComposition
{
    protected $season;

    protected $cleanup;

    protected $visitor;

    protected $bedlinen;

    public function __construct(Price $rent, Price $touristTax, Season $season, Price $cleanup, Price $visitor, Price $bedlinen)
    {
        parent::__construct($rent, $touristTax);
        $this->season = $season;
        $this->cleanup = $cleanup;
        $this->visitor = $visitor;
        $this->bedlinen = $bedlinen;
    }

    /**
     * @return Season
     */
    public function getSeason(): Season
    {
        return $this->season;
    }

    /**
     * @param Season $season
     */
    public function setSeason($season): void
    {
        if (!$season instanceof Season) {
            $season = new Season($season);
        }

        $this->season = $season;
    }

    /**
     * @return Price
     */
    public function getCleanup(): Price
    {
        return $this->cleanup;
    }

    /**
     * @param Price $cleanup
     */
    public function setCleanup(Price $cleanup): void
    {
        $this->cleanup = $cleanup;
    }

    /**
     * @return Price
     */
    public function getVisitor(): Price
    {
        return $this->visitor;
    }

    /**
     * @param Price $visitor
     */
    public function setVisitor(Price $visitor): void
    {
        $this->visitor = $visitor;
    }

    /**
     * @return Price
     */
    public function getBedlinen(): Price
    {
        return $this->bedlinen;
    }

    /**
     * @param Price $bedlinen
     */
    public function setBedlinen(Price $bedlinen): void
    {
        $this->bedlinen = $bedlinen;
    }
}