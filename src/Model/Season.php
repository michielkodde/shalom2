<?php

namespace App\Model;

final class Season
{
    const SEASON_HIGH = 'hoogseizoen';
    const SEASON_LOW = 'laagseizoen';

    /**
     * @var string
     */
    private $season;

    /**
     * @param string $season
     */
    public function __construct(string $season)
    {
        self::assertValidSeason($season);
        $this->season = $season;
    }

    /**
     * @return string
     */
    public function getSeason(): string
    {
        return $this->season;
    }

    /**
     * @param string $season
     */
    public function setSeason(string $season): void
    {
        self::assertValidSeason($season);
        $this->season = $season;
    }

    public static function assertValidSeason($proposedSeason)
    {
        if (!is_string($proposedSeason) || !in_array($proposedSeason, [self::SEASON_HIGH, self::SEASON_LOW])) {
            throw new \InvalidArgumentException('Season can be high or low');
        }
    }

    public function __toString()
    {
        return $this->getSeason();
    }
}