const loadGoogleMapsApi = require('load-google-maps-api')

$("img.lazy").lazyload({
    effect : "fadeIn",
    skip_invisible : true
});

$(document).ready(function() {

    // Bind to the click of all links with a #hash in the href
    $('a[href^="#"]').click(function(e) {
        // Prevent the jump and the #hash from appearing on the address bar
        e.preventDefault();
        // Scroll the window, stop any previous animation, stop on user manual scroll
        // Check https://github.com/flesler/jquery.scrollTo for more customizability

        var windowWidth = $(window).width();
        var subtraction = 400;
        if (windowWidth < 1024){
            subtraction = 50;
        }

        $(window).stop(true).scrollTo(($(this.hash).position().top - subtraction) + 'px', {duration:1000, interrupt:true});
    });

    $('.navbar-collapse a').on('click', function(){
        $('.navbar-toggle').click() //bootstrap 3.x by Richard
    });
});
if (document.querySelector('#map-nearby') !== null) {
    loadGoogleMapsApi().then(function (googleMaps) {
        const shalom = {
            lat: 51.567909,
            lng: 3.526279,
        };
        const mapNearby = new googleMaps.Map(document.querySelector('#map-nearby'), {
            center: shalom,
            zoom: 12,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: false
        });

        const marker = new google.maps.Marker({
            position: shalom,
            map: mapNearby
        });

        const infowindow = new google.maps.InfoWindow({
            content: '<p><a href="http://bit.ly/29tqzfx" target="_blank">Routebeschrijving naar<br>Minicamping Shalom</a></p>'
        });

        marker.addListener('click', function () {
            infowindow.open(mapNearby, marker);
        });

        google.maps.event.trigger(marker, 'click');

    }).catch(function (error) {
        console.error(error)
    });
}

