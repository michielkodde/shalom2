'use strict'
import 'jquery'

import './vendor/bootstrap.min';
import './vendor/jquery.scrollTo.min';
import './vendor/jquery.lazyload';

import './components/shalom';
import './components/gallery';
